//
//  ApiRouter.swift
//  KrakenX
//
//  Created by Betim S on 10/17/17.
//  Copyright © 2017 Betim S. All rights reserved.
//

import Foundation
import Alamofire

enum ApiRouter:URLRequestConvertible {
    case ticker
    case balance
    case openOrders
    case cancelOrder(txid:String)
    case addOrder(parameters: Parameters)
    
    static let baseURLString = "http://localhost:3500/api/"
    
    var method: HTTPMethod {
        switch self {
        case .ticker,.balance,.openOrders:
            return .get
        case .cancelOrder:
            return .put
        case .addOrder:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .ticker:
            return "ticker"
        case .balance:
            return "balance"
        case .openOrders:
            return "openorders"
        case .cancelOrder(let txid):
            return "cancelorder/\(txid)"
        case .addOrder:
            return "addorder"
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try ApiRouter.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .addOrder(let parameters):
            urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
        default:
            break
        }
        
        return urlRequest
    }
}

