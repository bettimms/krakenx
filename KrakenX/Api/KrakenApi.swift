//
//  KrakenApi.swift
//  KrakenX
//
//  Created by Betim S on 10/17/17.
//  Copyright © 2017 Betim S. All rights reserved.
//https://github.com/warren-bank/node-kraken-api

import Foundation
import Alamofire
import AlamofireObjectMapper


class KrakenApi {
    
    static let sharedInstance = KrakenApi()
    private init() {}
    let keyPath = ""
    
    func ticker(completionHandler: @escaping (Ticker) -> Void) {
        Alamofire.request(ApiRouter.ticker).responseObject(keyPath:"XXRPZEUR") {  (response: DataResponse<Ticker>) in
            if let json = response.result.value {
                completionHandler(json)
            }
        }
    }
    func balance(completionHandler:@escaping (Balance)->Void){
        Alamofire.request(ApiRouter.balance).responseObject { (response: DataResponse<Balance>) in
            if let json = response.result.value {
                completionHandler(json)
            }
        }
    }
    func openOrders(completionHandler:@escaping ([Order])->Void) {
        Alamofire.request(ApiRouter.openOrders).responseObject{ (response: DataResponse<OpenOrders>) in
            if let openOrders = response.result.value {
                let orders = self.extractOrders(openOrders: openOrders)
                completionHandler(orders)
            }
        }
    }
    func cancelOrder(txId:String,completionHandler:@escaping(DataResponse<Any>)->Void){
        Alamofire.request(ApiRouter.cancelOrder(txid: txId)).responseJSON { response in
            completionHandler(response)
            
        }
    }
    func addOrder(parameters:[String:String],completionHandler:@escaping(DataResponse<Any>)->Void){
        Alamofire.request(ApiRouter.addOrder(parameters: parameters)).responseJSON { response in
            completionHandler(response)
            
        }
    }
}

extension KrakenApi{
    private func extractOrders(openOrders:OpenOrders)->[Order]{
        var orders:[Order] = []
        if let allOrders = openOrders.open{
            for (key,order) in allOrders{
                order.orderId = key
                orders.append(order)
            }
        }
        return orders
    }
}

