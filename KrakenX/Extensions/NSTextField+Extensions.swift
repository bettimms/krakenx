//
//  NSTextField+Extensions.swift
//  KrakenX
//
//  Created by Betim S on 10/21/17.
//  Copyright © 2017 Betim S. All rights reserved.
//

import Cocoa


extension NSTextField{
    func decimatFormatter(){
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.isLenient = true
        formatter.usesSignificantDigits = true
        self.formatter = formatter
    }
}
extension String {
    func runAsCommand() -> String {
        let pipe = Pipe()
        let task = Process()
        task.launchPath = "/bin/sh"
        task.arguments = ["-c", String(format:"%@", self)]
        task.standardOutput = pipe
        let file = pipe.fileHandleForReading
        task.launch()
        if let result = NSString(data: file.readDataToEndOfFile(), encoding: String.Encoding.utf8.rawValue) {
            return result as String
        }
        else {
            return "--- Error running command - Unable to initialize string from file data ---"
        }
    }
}
