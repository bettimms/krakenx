//
//  NSView+Extensions.swift
//  MacKraken
//
//  Created by Betim S on 10/13/17.
//  Copyright © 2017 Betim S. All rights reserved.
//

import Cocoa

extension NSView {
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionReveal
        animation.duration = duration
        layer?.add(animation, forKey: kCATransitionReveal)
    }
    func shake() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 3
        animation.autoreverses = true
        let fromMake = CGPoint(x:self.frame.origin.x - 10, y:self.frame.origin.y)
        let toMake = CGPoint(x:self.frame.origin.x + 10,y: self.frame.origin.y)
        animation.fromValue = fromMake
        animation.toValue = toMake
        self.layer?.add(animation, forKey: "position")
    }
}
