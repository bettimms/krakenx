//
//  Double+Extensions.swift
//  MacKraken
//
//  Created by Betim S on 10/13/17.
//  Copyright © 2017 Betim S. All rights reserved.
//

import Foundation

extension Double {
    var convertAsLocaleCurrency :String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "en_IE")
        return formatter.string(from: self as NSNumber)!
    }
    func toCurrency(_ currency:String = "")->String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "en_IE")
        let localeFormatted = formatter.string(from: self as NSNumber)!
        let customFormatted = !currency.isEmpty ? "\(currency) \(String(localeFormatted.dropFirst()))" : String(localeFormatted.dropFirst())
        return customFormatted
    }
}
extension Double{
    var stringValue:String{
        return String(self)
    }
}
