//
//  NSStatusItem+Extensions.swift
//  KrakenX
//
//  Created by Betim S on 10/25/17.
//  Copyright © 2017 Betim S. All rights reserved.
//

import Cocoa

extension NSStatusItem{
    func updateTitle(lastTrade:Double, color:NSColor = NSColor.darkGray){
        
        let font = NSFont(name: "Arial", size: 14)
        let titleAttributes = NSDictionary(objects: [color,font!], forKeys: [NSAttributedStringKey.foregroundColor as NSCopying, NSAttributedStringKey.font as NSCopying])
        let title = NSAttributedString(string: String(format:"%.5f",lastTrade), attributes:titleAttributes as? [NSAttributedStringKey : Any] )
        self.attributedTitle = title
        //            self.statusItem.title = String(format:"%.5f",lastValue)
    }
}
