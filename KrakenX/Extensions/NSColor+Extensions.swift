//
//  NSColor+Extensions.swift
//  KrakenX
//
//  Created by Betim S on 10/23/17.
//  Copyright © 2017 Betim S. All rights reserved.
//

import Cocoa

extension NSColor{
    open class var greenButton:NSColor{
        get {return NSColor(red: 0, green: 184/255, blue: 106/255, alpha:1)}
    }
    open class var greenStatusItem:NSColor{
        get { return NSColor(red: 0.02, green: 0.55, blue: 0.24 , alpha: 1)}
    }
    open class var redButton:NSColor{
        get {return NSColor(red: 230/255, green: 0, blue: 0, alpha: 1)}
    }
    func from(r:Int,g:Int,b:Int) -> NSColor {
        return NSColor(red: CGFloat(r/255), green: CGFloat(g/255), blue: CGFloat(b/255), alpha: 1)
    }
    func highlitedColor(color:NSColor)->NSColor{
        return NSColor(red: color.redComponent, green: color.greenComponent, blue: color.blueComponent, alpha: 0.7)
    }
    
}
