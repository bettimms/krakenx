//
//  AppDelegate+Extensions.swift
//  KrakenX
//
//  Created by Betim S on 10/17/17.
//  Copyright © 2017 Betim S. All rights reserved.
//

import Cocoa

extension AppDelegate{
    func initStatusItem() {
        if let button = statusItem.button {
            statusItem.title = "0.000"
            //            button.image = NSImage(named:NSImage.Name("StatusBarButtonImage"))
            button.action = #selector(togglePopover(_:))
        }
        
        let popoverController = PopoverSplitViewController.freshController() as PopoverSplitViewController
        popoverController.statusItem = statusItem
        popover.contentViewController = popoverController
        
        if let balanceViewCtrl = popoverController.splitViewItems[0].viewController as? BalanceViewController{
            balanceViewCtrl.statusItem = self.statusItem
            balanceViewCtrl.initTicker()
        }
        
    }
    
    @objc func togglePopover(_ sender: Any?) {
        if popover.isShown {
            closePopover(sender: sender)
        } else {
            showPopover(sender: sender)
        }
    }
    
    func showPopover(sender: Any?) {
        if let button = statusItem.button {
            popover.show(relativeTo: button.bounds, of: button, preferredEdge: NSRectEdge.minY)
            eventMonitor?.start()
        }
    }
    
    func closePopover(sender: Any?) {
        popover.performClose(sender)
        eventMonitor?.stop()
    }
    
    func updateOrder(_ orders:[Order]){
        
//        self.subviews.removeAll()
//
//        let ySpaceBetweenViews = 28
//        let containerHeight = (orders.count * 55)
//        self.frame = CGRect(x: 0, y: 0, width: Int(self.frame.width), height: containerHeight)
//
//        for (index,order) in orders.enumerated() {
//            let offset = CGFloat(index * ySpaceBetweenViews)
//            let frame = CGRect(x:0, y:offset, width:self.frame.width, height:self.frame.height)
//
//            let openOrderView = OpenOrderView(frame: frame)
//            openOrderView.viewId = index
//            self.addSubview(openOrderView)
//            openOrderView.setData(order: order)
//        }
    }
}
