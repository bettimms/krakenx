//
//  AddOrderViewController.swift
//  KrakenX
//
//  Created by Betim S on 10/20/17.
//  Copyright © 2017 Betim S. All rights reserved.
//

import Cocoa

class AddOrderViewController: NSViewController {
    
    var timer  = Timer()
    var ticker:Ticker?
    var amount:Double = 0
    var limit:Double = 0
    var total:Double = 0
    var lastModified = 0
    var orderViewCtrl:OpenOrderViewController?
    @IBOutlet weak var submitButton: CustomButton!
    
    @IBOutlet weak var tradeTypeSegmentedControl: NSSegmentedControl!
    @IBOutlet weak var orderTypeSegmentedControl: NSSegmentedControl!
    @IBOutlet weak var amountTextField: NSTextField!
    @IBOutlet weak var orderTypeTextField: NSTextField!
    @IBOutlet weak var totalTextField: NSTextField!
    @IBOutlet weak var statusTextField: NSTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.preferredContentSize = NSMakeSize(self.view.frame.size.width, self.view.frame.size.height)
        
        amountTextField.delegate = self
        orderTypeTextField.delegate = self
        totalTextField.delegate = self
        
        amountTextField.decimatFormatter()
        totalTextField.decimatFormatter()
        
        orderViewCtrl = self.splitViewItemController(index: 1) as? OpenOrderViewController
        setSubmitButtonTitle()
    }
    func setSubmitButtonTitle(){
        if tradeTypeSegmentedControl.selectedSegment == Trade.Buy{
            submitButton.title = "Buy"
            submitButton.blockColor = NSColor.greenButton
            
        }else{
            submitButton.title = "Sell"
            submitButton.blockColor = NSColor.redButton
        }
    }
    @IBAction func onTrade(_ sender: Any) {
        setSubmitButtonTitle()
    }
    @IBAction func onTradeType(_ sender: Any) {
        if orderTypeSegmentedControl.selectedSegment == OrderType.Market && ticker != nil{
            orderTypeTextField.stringValue = (ticker?.lastTrade![0].stringValue)!
            limit = (ticker?.lastTrade![0])!
        }else{
            orderTypeTextField.stringValue = ""
            limit = 0
        }
        recalculate()
    }
    @IBAction func onSubmit(_ sender: Any) {
        let type = tradeTypeSegmentedControl.selectedSegment == Trade.Buy ? "buy" : "sell"
        let orderType = orderTypeSegmentedControl.selectedSegment == OrderType.Limit ? "limit" : "market"
        
        let newOrder = [
            "pair": "XXRPZEUR",
            "type": type,
            "ordertype": orderType,
            "price":limit.stringValue,
            "volume": amountTextField.stringValue
        ]
        resetTimer()
        self.statusTextField.stringValue = "Waiting..."
        KrakenApi.sharedInstance.addOrder(parameters: newOrder, completionHandler: {(response)->Void in
            switch response.result {
            case .success:
                print("SUCCESS")
                self.statusTextField.stringValue = "Success"
                self.orderViewCtrl?.getOrders()
            case .failure:
                self.statusTextField.stringValue = "Failed"
                self.statusTextField.textColor = NSColor.red
                self.timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.resetTimer), userInfo: nil, repeats: false)
                RunLoop.current.add(self.timer, forMode: RunLoopMode.commonModes)
                print("FAILED",response)
            }
        })
    }
    @objc func resetTimer(){
        statusTextField.stringValue = ""
        statusTextField.textColor = NSColor.systemGreen
        timer.invalidate()
    }
    
}


extension AddOrderViewController:NSTextFieldDelegate{
    fileprivate enum TextFieldType{
        static let Amount = 100
        static let OrderType = 101
        static let Total = 102
    }
    fileprivate enum OrderType{
        static let Market = 0
        static let Limit = 1
    }
    fileprivate enum Trade{
        static let Buy = 0
        static let Sell = 1
    }
    func recalculate(){
        if amount > 0 && lastModified == TextFieldType.Amount {
            totalTextField.stringValue  = String(describing: (amount * limit))
        }else if total > 0 && lastModified == TextFieldType.Total && limit != 0 {
            amountTextField.stringValue  = String(describing: (total / limit))
        }
    }
    override func controlTextDidChange(_ obj: Notification) {
        if let textField  = obj.object as? NSTextField{
            if textField.tag == TextFieldType.Amount{
                amount = textField.doubleValue
                totalTextField.stringValue  = String(describing: (amount * limit))
                lastModified = TextFieldType.Amount
            }else if textField.tag == TextFieldType.OrderType {
                orderTypeSegmentedControl.selectSegment(withTag: 1)
                limit = textField.doubleValue
                recalculate()
                
            }else if textField.tag == TextFieldType.Total {
                total = textField.doubleValue
                amountTextField.stringValue  = String(describing: (total / limit))
                lastModified = TextFieldType.Total
            }
        }
    }
    
    
}
