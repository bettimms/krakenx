//
//  QuitViewController.swift
//  KrakenX
//
//  Created by Betim S on 10/20/17.
//  Copyright © 2017 Betim S. All rights reserved.
//

import Cocoa

class QuitViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
         self.preferredContentSize = NSMakeSize(self.view.frame.size.width, self.view.frame.size.height)
    }
   
    @IBAction func onQuit(_ sender: Any) {
        NSApplication.shared.terminate(self)
    }
    
}
