//
//  BalanceViewController.swift
//  KrakenX
//
//  Created by Betim S on 10/17/17.
//  Copyright © 2017 Betim S. All rights reserved.
//119617

import Cocoa

public let initialInvestment  = 21000+1754.38
class BalanceViewController: NSViewController {

    var timer = Timer()
    var statusItem:NSStatusItem!//From AppDelegate
    var previousValue:Double = 0
    let tickerFetchingTime:Double = 15.0 //time in seconds
    
    @IBOutlet weak var lastTradeTextField: NSTextField!
    @IBOutlet weak var balanceDigitalTextField: NSTextField!
    
    @IBOutlet weak var balanceFiatTextField: NSTextField!
    @IBOutlet weak var totalFiatTextField: NSTextField!
    @IBOutlet weak var profitTextFiled: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let parent = self.parent as! PopoverSplitViewController
        statusItem = parent.statusItem        
        //https://stackoverflow.com/questions/33525323/nsviewcontroller-disable-resize
         self.preferredContentSize = NSMakeSize(self.view.frame.size.width, self.view.frame.size.height)
        
       initTicker()
    }
    func initTicker(){
        updateCounting()
        self.timer = Timer.scheduledTimer(timeInterval: tickerFetchingTime, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
        RunLoop.current.add(self.timer, forMode: RunLoopMode.commonModes)
    }
    @objc func updateCounting(){
        KrakenApi.sharedInstance.ticker(completionHandler: {(ticker)->Void in
            let lastTrade = ticker.lastTrade![0]
            let color = self.previousValue>lastTrade ? NSColor.red : NSColor.greenStatusItem
            self.statusItem?.updateTitle(lastTrade: lastTrade,color: color)
            
            let addOrderViewCtrl = self.splitViewItemController(index: 2) as! AddOrderViewController
            addOrderViewCtrl.ticker = ticker
            
            KrakenApi.sharedInstance.balance(completionHandler: {(balance)->Void in
                let model = KrakenModel(lastTrade: lastTrade, balanceFiat: balance.eur!, balanceCrypto: balance.xrp!)
                if(self.previousValue != lastTrade){
                    self.updateBalanceDetails(model:model)
                }
                self.previousValue = lastTrade
            })
        })
    }    
}

extension BalanceViewController{
    
    func updateBalanceDetails(model:KrakenModel){
        
        //Called from AppDelegate (Not initialized yet)
        if !(self.lastTradeTextField != nil){
            return
        }
        let color = self.previousValue>model.lastTrade ? NSColor.red : NSColor.greenStatusItem
        DispatchQueue.main.async {
            self.lastTradeTextField.stringValue = String(format:"%.5f",model.lastTrade)
            self.balanceDigitalTextField.stringValue = model.balanceCrypto.toCurrency("Ʀ")
            self.balanceFiatTextField.stringValue = model.balanceFiat.convertAsLocaleCurrency
            self.totalFiatTextField.stringValue = model.totalFiat
            self.profitTextFiled.stringValue = (model.total - initialInvestment).convertAsLocaleCurrency
            
            self.lastTradeTextField.textColor = color
            self.balanceDigitalTextField.textColor = color
            self.totalFiatTextField.textColor = color
            
            
            NSAnimationContext.runAnimationGroup({ (context) -> Void in
                context.duration = 0.3
                self.lastTradeTextField.animator().frameRotation = 3
                self.totalFiatTextField.animator().frameRotation = 3
            }, completionHandler: { () -> Void in
                self.lastTradeTextField.animator().frameRotation = 0
                self.totalFiatTextField.animator().frameRotation = 0
            })
            
        }
    }
}
