//
//  OpenOrderViewController.swift
//  KrakenX
//
//  Created by Betim S on 10/17/17.
//  Copyright © 2017 Betim S. All rights reserved.
//
//https://source.ind.ie/project/heartbeat-cocoa/blob/69f3c061f42c7ec68b1f625420f6099ea82539a4/Heartbeat/IndieSplitViewController.swift
import Cocoa

class OpenOrderViewController: NSViewController{
    
    var timer = Timer()
    var popover:NSPopover?
    
    //The value from Main.storyboard row height
    var rowHeight: CGFloat = 80 {
        didSet {
            if self.orders.count != 0{
                rowHeight = CGFloat(self.orders.count*80)
            }
        }
    }
    
    @IBOutlet weak var tableView: NSTableView!
    var orders:[Order] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        popover?.contentSize = CGSize(width: Int(self.view.frame.width), height: Int(self.view.frame.height))
        tableView.delegate = self
        tableView.dataSource = self
        self.isCollapsed(true,animated: false)
        getOrders()
        
    }
    func getOrders(){
        KrakenApi.sharedInstance.openOrders(completionHandler: {(orders)->Void in
            self.orders = orders
            
            self.preferredContentSize = NSMakeSize(self.view.frame.size.width, self.rowHeight)
            self.tableView.reloadData()
            if orders.count > 0 {
                self.isCollapsed(false)
            }
        })
    }
    @IBAction func removeCell(_ sender: Any) {
        let button = sender as! NSButton
        
        if let index = orders.index(where: {$0.orderId == button.title}){
            if let cell = tableView.view(atColumn: 0, row: index, makeIfNecessary: false) as? OrderTableCellView{
//                print(cell.orderIdButton.title)
                removeOrder(orderCell: cell,onSuccess: {
                    self.orders.remove(at: index)
                    self.tableView.reloadData()
                    if self.orders.count == 0{
                        self.isCollapsed(true)
                    }
                    self.preferredContentSize = NSMakeSize(self.view.frame.size.width, self.rowHeight)
                })
            }
        }
    }
    
    func removeOrder(orderCell:OrderTableCellView, onSuccess:@escaping ()->Void){
        resetCancalationStatus(textField: orderCell.cancalationStatusTextField)
        orderCell.cancalationStatusTextField.stringValue = "Waiting..."
        KrakenApi.sharedInstance.cancelOrder(txId: orderCell.orderIdButton.title, completionHandler: {(response)->Void in
            switch response.result {
           case .success:
               orderCell.cancalationStatusTextField.stringValue = "Success"
               onSuccess()
           case .failure:
               print(response)
               orderCell.cancalationStatusTextField.stringValue = "Failed"
               orderCell.cancalationStatusTextField.textColor = NSColor.red
               self.timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.resetTimer), userInfo: orderCell, repeats: false)
               RunLoop.current.add(self.timer, forMode: RunLoopMode.commonModes)
           }
        })
    }
    @objc func resetTimer(_ timer: Timer){
          let orderCell = timer.userInfo as! OrderTableCellView
          resetCancalationStatus(textField: orderCell.cancalationStatusTextField)
          self.timer.invalidate()
    }
    func resetCancalationStatus(textField:NSTextField){
        textField.stringValue = ""
        textField.textColor = NSColor.systemGreen
    }
}



extension OpenOrderViewController:NSTableViewDelegate,NSTableViewDataSource{
    
    fileprivate enum CellIdentifiers{
        static let TypeCell = "TypeCellId"
        static let DescriptionCell = "DescriptionCellId"
        static let CostCell = "CostCellId"
    }
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return orders.count
    }
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
       
        
        if let cell = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "OrderCellIdentifier"), owner: self) as? OrderTableCellView {
            let order = orders[row]
            //TODO Fix to return dynamically the currencyType
//            let currencyType = order.type! == "sell" ? "EUR:" : "XRP:"
            
            cell.orderIdButton.title = order.orderId!
            cell.orderTypeTextField.stringValue = order.type!
            cell.priceTextField.stringValue = (order.price?.stringValue)!
            cell.profitTextField.stringValue = (order.price!*order.volume! - initialInvestment).convertAsLocaleCurrency
            
            cell.tradeMessageTextField.stringValue = "\((order.volume!-order.volumeExecuted!).toCurrency("XRP:"))       Cost: \((order.volume!*order.price!-order.cost!).convertAsLocaleCurrency)"
//            if tableColumn == tableView.tableColumns[0] {
//                cell.textField?.stringValue = order.type!
//            }else if tableColumn == tableView.tableColumns[1]{
//                cell.textField?.stringValue = order.order!
//            }else if tableColumn == tableView.tableColumns[2]{
//                cell.textField?.stringValue = (order.price?.convertAsLocaleCurrency)!
//            }
            return cell
        }
        return nil
    }
}




extension NSViewController{
    func isCollapsed(_ collapsed:Bool, animated:Bool = true){
        
        splitViewItem.collapseBehavior = .preferResizingSplitViewWithFixedSiblings
        
        //Fix the problem of collapse by calling in main.async
        DispatchQueue.main.async {
            if animated{
                self.splitViewItem.animator().isCollapsed = collapsed
            }else {
                self.splitViewItem.isCollapsed = collapsed
            }
        }
    }
    
    var splitViewItem:NSSplitViewItem{
        let mainSplitViewCtrl = self.parent as! PopoverSplitViewController
        let splitViewItem = mainSplitViewCtrl.splitViewItem(for: self)
        return splitViewItem!
    }
    func splitViewItemController(index:Int) -> NSViewController {
        let mainSplitViewCtrl = self.parent as! PopoverSplitViewController
        let viewCtrl = mainSplitViewCtrl.splitViewItems[index].viewController
        return viewCtrl
    }
}




