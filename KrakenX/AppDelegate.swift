//
//  AppDelegate.swift
//  KrakenX
//
//  Created by Betim S on 10/17/17.
//  Copyright © 2017 Betim S. All rights reserved.
//To enable localhost in info.plist
//https://gist.github.com/briancroom/a61dfb6dd0593da353ed
//Start at login
//https://github.com/atjason/CocoaDemoWithSwift/tree/master/Preferences
import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    var eventMonitor: EventMonitor?
    let statusItem = NSStatusBar.system.statusItem(withLength:NSStatusItem.variableLength)
    let popover = NSPopover()

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        statusItem.highlightMode = false
        
        eventMonitor = EventMonitor(mask: [.leftMouseDown, .rightMouseDown]) { [weak self] event in
            if let strongSelf = self, strongSelf.popover.isShown {
                strongSelf.closePopover(sender: event)
            }
        }
        initStatusItem()
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
}
