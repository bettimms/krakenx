//
//  KrakenModel.swift
//  MacKraken
//
//  Created by Betim S on 10/13/17.
//  Copyright © 2017 Betim S. All rights reserved.
//

import Foundation

struct KrakenModel {
    var lastTrade:Double
    var balanceFiat:Double
    var balanceCrypto:Double
    
    var total:Double{
        return ((balanceCrypto * lastTrade)+balanceFiat)
    }
    var totalFiat:String{
        return self.total.convertAsLocaleCurrency
    }
}
