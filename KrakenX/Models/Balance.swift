//
//  Balance.swift
//  MacKraken
//
//  Created by Betim S on 10/15/17.
//  Copyright © 2017 Betim S. All rights reserved.
//

//a = ask array(<price>, <whole lot volume>, <lot volume>),
//b = bid array(<price>, <whole lot volume>, <lot volume>),
//c = last trade closed array(<price>, <lot volume>),
//v = volume array(<today>, <last 24 hours>),
//p = volume weighted average price array(<today>, <last 24 hours>),
//t = number of trades array(<today>, <last 24 hours>),
//l = low array(<today>, <last 24 hours>),
//h = high array(<today>, <last 24 hours>),
//o = today's opening price

import Foundation
import ObjectMapper

class Balance: Mappable {
    var xrp:Double?
    var eur:Double?
    
    required init?(map: Map) {        
    }
    
    func mapping(map: Map) {
        let transform = TransformOf<Double, String>(fromJSON: { (value: String?) -> Double? in
            if let v = value {
                return Double(v)
            }
            return nil
        }, toJSON: { (value: Double?) -> String? in
            // transform value from Double? to String?
            if let value = value {
                return String(value)
            }
            return nil
        })
        xrp <- (map["XXRP"],transform)
        eur <- (map["ZEUR"],transform)
    }
}
