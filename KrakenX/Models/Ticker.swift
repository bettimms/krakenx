//
//  Ticker.swift
//  MacKraken
//
//  Created by Betim S on 10/15/17.
//  Copyright © 2017 Betim S. All rights reserved.
//

//a = ask array(<price>, <whole lot volume>, <lot volume>),
//b = bid array(<price>, <whole lot volume>, <lot volume>),
//c = last trade closed array(<price>, <lot volume>),
//v = volume array(<today>, <last 24 hours>),
//p = volume weighted average price array(<today>, <last 24 hours>),
//t = number of trades array(<today>, <last 24 hours>),
//l = low array(<today>, <last 24 hours>),
//h = high array(<today>, <last 24 hours>),
//o = today's opening price

import Foundation
import ObjectMapper

class Ticker: Mappable {
    var lastTrade:[Double]?
    var lowTrade:[Double]?
    var highTrade:[Double]?
    
    required init?(map: Map) {        
    }
    
    func mapping(map: Map) {
//        print(map.JSON)
        let transform = TransformOf<[Double], [String]>(fromJSON: { (values: [String]?) -> [Double]? in
            if let val = values {
                return val.map{Double($0)!}
            }
            return nil
        }, toJSON: { (values: [Double]?) -> [String]? in
            // transform value from Double? to String?
            if let val = values {
                return val.map{String($0)}
            }
            return nil
        })
        
        lastTrade <- (map["c"],transform)
        lowTrade <- (map["l"],transform)
        highTrade <- (map["h"],transform)
        
//        lastTrade <- map["c"]
//        lowTrade <- map["l"]
//        highTrade <- map["h"]
        
    }
}
