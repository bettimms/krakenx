//
//  OpenOrder.swift
//  MacKraken
//
//  Created by Betim S on 10/14/17.
//  Copyright © 2017 Betim S. All rights reserved.
//
//https://stackoverflow.com/questions/44806608/objectmapper-nested-dynamic-keys
//https://stackoverflow.com/questions/33658825/swift-how-to-convert-json-with-dynamic-keys-with-objectmapper
import Foundation
import ObjectMapper

class OpenOrders: Mappable {
    var open:[String:Order]?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        open <- map["open"]
    }
}
class Order:Mappable {
    var orderId:String?
    var order:String?
    var type:String?
    var price:Double?
    var volume:Double?
    var volumeExecuted:Double?
    var cost:Double?
    var status:String?
    var pair:String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        let transform = TransformOf<Double, String>(fromJSON: { (value: String?) -> Double? in
            if let v = value {
                return Double(v)
            }
            return nil
        }, toJSON: { (value: Double?) -> String? in
            // transform value from Double? to String?
            if let value = value {
                return String(value)
            }
            return nil
        })
        
        volume <- (map["vol"],transform)
        volumeExecuted <- (map["vol_exec"],transform)
        cost <- (map["cost"],transform)
        status <- map["status"]
        order <- map["descr.order"]
        type <- map["descr.type"]
        pair <- map["descr.pair"]
        price <- (map["descr.price"],transform)
    }
    
   
    
}
