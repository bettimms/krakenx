//
//  OrderTableCellView.swift
//  KrakenX
//
//  Created by Betim S on 10/20/17.
//  Copyright © 2017 Betim S. All rights reserved.
//

import Cocoa

class OrderTableCellView: NSTableCellView {

    @IBOutlet weak var orderIdButton: NSButton!
    @IBOutlet weak var orderTypeTextField: NSTextField!
    @IBOutlet weak var priceTextField: NSTextField!
    @IBOutlet weak var cancalationStatusTextField: NSTextField!
    @IBOutlet weak var tradeMessageTextField: NSTextField!
    @IBOutlet weak var profitTextField: NSTextField!
    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }
    
}
