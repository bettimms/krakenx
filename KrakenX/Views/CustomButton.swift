//
//  CustomButton.swift
//  KrakenX
//
//  Created by Betim S on 10/23/17.
//  Copyright © 2017 Betim S. All rights reserved.
//https://medium.com/bpxl-craft/working-with-ibdesignable-e8318a2c3e55

import Cocoa

@IBDesignable
class CustomButton:NSButton{
    var titleParagraphStyle:NSMutableParagraphStyle?
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.wantsLayer = true
        titleParagraphStyle = NSMutableParagraphStyle()
        titleParagraphStyle?.alignment = .center
    }
    override var wantsUpdateLayer: Bool{
        get { return true}
    }
    override func updateLayer() {
        if (self.cell?.isHighlighted)! {
            layer?.backgroundColor = self.blockColor.highlitedColor(color: self.blockColor).cgColor
        }else{
            layer?.backgroundColor = self.blockColor.cgColor
        }
    }
    
    @IBInspectable var cornerRounding: CGFloat = 10 {
        didSet {
            layer?.cornerRadius = cornerRounding
        }
    }
    @IBInspectable var color: NSColor = NSColor.white {
        didSet {
            self.attributedTitle = NSAttributedString(string: self.title,attributes: [NSAttributedStringKey.foregroundColor : self.color, NSAttributedStringKey.paragraphStyle:titleParagraphStyle!])
        }
    }
    @IBInspectable var blockColor: NSColor = NSColor.gray {
        didSet {
            layer?.backgroundColor = blockColor.cgColor
            self.attributedTitle = NSAttributedString(string: self.title,attributes: [NSAttributedStringKey.foregroundColor : self.color, NSAttributedStringKey.paragraphStyle:titleParagraphStyle!])
        }
    }
    @IBInspectable var hightlightColor: NSColor = NSColor.black {
        didSet {
            
        }
    }
    
    override func prepareForInterfaceBuilder() {
        layer?.backgroundColor = blockColor.cgColor
        layer?.cornerRadius = cornerRounding
    }
    
    override func awakeFromNib() {
        layer?.backgroundColor = blockColor.cgColor
        layer?.cornerRadius = cornerRounding
        
        self.attributedTitle = NSAttributedString(string: self.title,attributes: [NSAttributedStringKey.foregroundColor : self.color, NSAttributedStringKey.paragraphStyle:titleParagraphStyle!])
        self.layer?.contentsCenter = NSRect(x: 0, y: 0, width: self.frame.width/2, height: self.frame.height/2)
    }
}

